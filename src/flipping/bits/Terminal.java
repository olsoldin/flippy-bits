package flipping.bits;

import java.awt.*;
import javax.swing.*;

/**
 * Simple fake terminal class
 * <p>
 * @author Oliver
 */
public class Terminal extends JFrame {

	private final JTextArea terminalText; //where the text will be
	private final JScrollPane terminalFrame; //adds a scrollbar to the text area

	/**
	 * Only exists because you cant open a terminal to print to from a java
	 * program unless it starts a new one and passes arguments to a separate
	 * program running in a separate terminal, much easier and safer to create a
	 * fake terminal from within the program
	 */
	public Terminal() {
		terminalText = new JTextArea();
		terminalFrame = new JScrollPane();

		setTitle("UltraTerminal3000 - Flippy_Bits.jar");
		setPreferredSize(new Dimension(700, 350));

		terminalText.setEditable(false);
		terminalText.setBackground(Color.BLACK);
		terminalText.setFont(new Font("Monospaced", 1, 12));
		terminalText.setForeground(new Color(204, 204, 204));
		terminalFrame.setViewportView(terminalText);

		add(terminalFrame);
		pack();
		isVisible();
	}

	/**
	 * Prints a blank line to the terminal
	 */
	public void println() {
		println("");
	}

	/**
	 * Prints the object passed to the terminal with a new line following it
	 * <p>
	 * @param obj the toString() method is called and printed to the terminal
	 *            followed by a new line
	 */
	public void println(Object obj) {
		print(obj + "\n");
	}

	/**
	 * Prints the passed object to the terminal
	 * <p>
	 * @param obj the toString() method is called and the result is added to the
	 *            end of the terminal
	 */
	public void print(Object obj) {
		terminalText.append(obj.toString());
	}

}
