package flipping.bits;

import java.awt.Color;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import javax.swing.JFrame;

/**
 *
 * @author Oliver
 */
public class FlippingBitsStart {

	private static final double VERSION = 6.1;
	private static final String USAGE = "Usage: java -jar Flipping_Bits.jar -s \"Size\"";
	private static boolean RANDOM = true, TERMINAL = false;
	private static int SIZE;
	private static Color ONCOLOUR = Color.ORANGE;
	private static Color OFFCOLOUR = Color.YELLOW;
	private static PrintStream PS;
	private static long SEED = Startup.SEED;

	/**
	 * Starts the FlippingBits program with the required parameters if the size
	 * is not specified, the game will start with default parameters
	 * <p>
	 * @param args the arguments passed through the command line, if the game
	 *             was run by double clicking then the arguments will be empty
	 */
	//@TODO: auto completer
	//@TODO: program startup without designer
	public static void main(String[] args) {
		/*
		 * If the args length is a positive number and it includes the size,
		 * then create a new game with the passed parameters, or 
		 */
		if (args.length > 0) {
			if (!canStartGame(args)) {//if the params dont contain the size, then tell the user
				System.out.println(USAGE);
				FlippingBitsStart.main(new String[]{});
				return;
			}
			SIZE = 5;//default size if anythnig goes wrong
			parseArgs(args);

			//Creates the frame to contain the game
			JFrame frame = new JFrame("Flippy Bits");
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

			//Creates the game and adds it to the frame
			FlippingBits fb = new FlippingBits(RANDOM, TERMINAL, PS, ONCOLOUR, OFFCOLOUR, SIZE, SEED);
			frame.setContentPane(fb);
			frame.pack();
			frame.setVisible(true);
			frame.setLocation(500, 300);
		} else {
			//Starts the startup form
			Startup st = new Startup();
			st.setVisible(true);
		}
	}

	/**
	 * Checks if either the size or the help option is passed
	 * <p>
	 * @param args the arguments to check
	 * <p>
	 * @return true if the size is set or the help option is active
	 */
	private static boolean canStartGame(String[] args) {
		for (String s : args) {
			if (s.equals("-s") || s.equals("--size") || s.equals("-h") || s.equals("--help")) {
				return true;
			}
		}
		return false;
	}

	//<editor-fold desc="Parsing arguments--WIP" defaultstate="collapsed">
	/**
	 * Takes the arguments given and checks for patterns and processes them
	 * accordingly
	 * <p>
	 * @param args
	 */
	private static void parseArgs(String[] args) {
		for (int i = 0; i < args.length; i++) {
			if (args[i].startsWith("-")) {
				switch (args[i]) {
					case "-h":
					case "--help":
						System.out.println("************Help************");
						System.out.println(USAGE);
						System.out.println("Arguments:");
						System.out.println("-h --help        Shows this help menu\n");
						System.out.println("-v --version     Shows the version of  the program");
						System.out.println("-s --size        Sets the size of the grid, can be any +ve integer");
						System.out.println("-n --norandom    If this argument is given, the board wont be randomised");
						System.out.println("-o --oncolour    Sets the on rgb of the button, given a 3 byte int");
						System.out.println("-c --offcolour   Sets the off rgb of the button, given a 3 byte int");
						System.out.println("-t --terminal   Outputs all the  data to the terminal");
						System.out.println("-f --file       Outputs all data to the specified file");
						System.out.println("-r --randomseed Sets the seed for the randomisation");
						System.out.println("****************************");
						break;
					case "-v":
					case "--version":
						System.out.println(getVersion());
						break;
					case "-s":
					case "--size":
						try {
							SIZE = Integer.parseInt(args[i + 1]);
						} catch (IndexOutOfBoundsException | NumberFormatException e) {
							System.out.println(e.getClass().getSimpleName());
						}
						break;
					case "-n":
					case "--norandom":
						RANDOM = false;
						break;
					case "-t":
					case "--terminal":
						TERMINAL = true;
						break;
					case "-f":
					case "--file":
						try {
							PS = new PrintStream(args[i + 1]);
						} catch (FileNotFoundException | IndexOutOfBoundsException e) {
							System.out.println("File \"" + args[i + 1] + "\" not found, setting output to default \"output.txt\"");
							try {
								PS = new PrintStream("output.txt");
							} catch (FileNotFoundException ex) {
								System.err.println("Fatal error, something went really wrong\n" + ex);
							}
						}
						break;
					case "-o":
					case "--oncolour":
						try {
							ONCOLOUR = new Color(Integer.valueOf(args[i + 1].substring(2), 16));
						} catch (IndexOutOfBoundsException | NumberFormatException e) {
							System.out.println(e.getClass().getSimpleName());
							System.out.println("Setting setting on colour to " + ONCOLOUR.toString());
						}
						break;
					case "-c":
					case "--offcolour":
						try {
							OFFCOLOUR = new Color(Integer.valueOf(args[i + 1].substring(2), 16));
						} catch (IndexOutOfBoundsException | NumberFormatException e) {
							System.out.println(e.getClass().getSimpleName());
							System.out.println("Setting setting on colour to " + OFFCOLOUR.toString());
						}
						break;
					case "-r":
					case "--randomseed":
						try {
							SEED = Integer.parseInt(args[i + 1]);
						} catch (NumberFormatException | IndexOutOfBoundsException e) {
							System.out.println(e.getClass().getSimpleName());
							System.out.println("Continuing with random seed");
						}
				}//end switch args[i]
			}//end if args.startsWith("-")
			System.out.println();
		}//end for i < args.length
	}

	/**
	 * The version and name of the game
	 * <p>
	 * @return version and name in the form
	 * <tt>FlippingBits v1.0 written by Oliver Olding</tt>
	 */
	public static String getVersion() {
		return FlippingBits.class.getSimpleName() + " v" + VERSION
				+ " written by Oliver Olding";
	}
    //</editor-fold>
}
