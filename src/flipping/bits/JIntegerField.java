/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package flipping.bits;

import javax.swing.JTextField;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.PlainDocument;

/**
 * A textbox that can only take an integer as an input, nothing else will be
 * allowed to be entered
 * <p>
 * @author Oliver Olding
 */
public class JIntegerField extends JTextField {

	/**
	 * Creates a new default textbox
	 */
	public JIntegerField() {
		super();
	}

	/**
	 * Creates a new default textbox with some text already in it Allowing this
	 * text to be anything allows you to have a textbox which says something
	 * like "Enter a whole number" and only when the user inputs something will
	 * it require an integer
	 * <p>
	 * @param text The text displayed in the textbox when it's created
	 */
	public JIntegerField(String text) {
		super(text);
	}

	/**
	 * Creates a new default textbox with '<b>columns</b>' positions to enter
	 * numbers
	 * <p>
	 * @param columns The number or columns
	 */
	public JIntegerField(int columns) {
		super(columns);
	}

	/**
	 * Creates a new default text area with <b>text</b> displayed, and being
	 * able to hold <b>columns</b> characters
	 * <p>
	 * @param text    the text to be displayed when it is created
	 * @param columns how many columns of numbers / chars can be in the box
	 */
	public JIntegerField(String text, int columns) {
		super(text, columns);
	}

	/**
	 *
	 * @return the Integer value of the textbox
	 */
	public int getInt() {
		//only allows adding ints so no need to surround with try and catch
		return Integer.parseInt(getText());
	}

	@Override
	protected Document createDefaultModel() {
		return new UpperCaseDocument();
	}

	static class UpperCaseDocument extends PlainDocument {

		@Override
		public void insertString(int offs, String str, AttributeSet a) throws BadLocationException {
			if (str == null) {
				return;
			}
			try {
				Integer.parseInt(str);
				super.insertString(offs, str, a);
			} catch (NumberFormatException e) {
			}
		}
	}
}
