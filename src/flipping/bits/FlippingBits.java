package flipping.bits;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.PrintStream;
import java.util.Random;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author Oliver
 */
public class FlippingBits extends JPanel {

	private final boolean terminal;
	private final PrintStream ps;
	private final Color onColour, offColour;
	private final int size;
	private final boolean[] grid;
	private long seed;
	//--//
	private JButton[][] jButton;

	/**
	 *
	 * @param random    true if the game starts with a random (solvable) grid
	 * @param terminal  true if the game outputs all the moves to a terminal
	 * @param ps        the print stream to output all the moves to
	 * @param onColour  the colour of a bit with the value of 1
	 * @param offColour the colour of a bit with the value of 0
	 * @param size      the size of the grid, a size 's' will give an 's x s'
	 *                  grid
	 * @param seed      the seed to make randomise the grid to
	 */
	public FlippingBits(boolean random, boolean terminal, PrintStream ps, Color onColour, Color offColour, int size, long seed) {
		this.terminal = terminal;
		this.ps = ps;
		this.onColour = onColour;
		this.offColour = offColour;
		this.size = size;
		this.seed = seed;
		grid = new boolean[size * size];
		initComponents();
		if (random) {
			println("Seed: " + seed);
			randomiseGrid();
		}
		printGrid();
		updateButtons();
	}

	/**
	 * Loops through all the bits and chooses them all, and updates the buttons
	 */
	public void testAll() {
		for (int i = 0; i < size * size; i++) {
			choose(i);
			updateButtons();
		}
	}

	/**
	 * Choose a bit to flip, in the range 0-9 This bit is flipped, along with
	 * the bits directly adjacent to it
	 * <p>
	 * @param flipBit The bit to flip, in range 0-9
	 */
	private void choose(int flipBit) {
		grid[flipBit] ^= true;//toggles true and false
		//makes sure all the adjacent bits exist
		if (flipBit > size - 1) {
			grid[flipBit - size] ^= true;
		}
		if (flipBit < size * (size - 1)) {
			grid[flipBit + size] ^= true;
		}
		if (flipBit % size < size - 1) {
			grid[flipBit + 1] ^= true;
		}
		if (flipBit % size > 0) {
			grid[flipBit - 1] ^= true;
		}
	}

	/**
	 * If all the bits are the same (all on or all off) then display a win
	 * dialog to give the player the option of restarting with or without a
	 * random grid
	 */
	//@TODO: change it to work if there is no GUI
	private void checkWin() {
		boolean winOn = true;
		boolean winOff = true;
		for (boolean b : grid) {
			winOn &= b;
			winOff &= !b;
		}
		if (winOn | winOff) {//if they are all on, or they are all off
			switch (JOptionPane.showConfirmDialog(null, "You win! Congratulations!\nPlay again with a different seed?",
					"You Win!",
					JOptionPane.YES_NO_CANCEL_OPTION)) {
				case JOptionPane.YES_OPTION://use a different seed
					seed = System.nanoTime() % 20000;
					randomiseGrid();
					break;
				case JOptionPane.NO_OPTION://use the same seed
					randomiseGrid();
					break;
				case JOptionPane.CANCEL_OPTION://reset the grid to its default state
					resetGrid();
					break;
			}
			updateButtons();
		}
	}

	/**
	 * Loops through a number of times and flips a random bit each time
	 */
	private void randomiseGrid() {
		Random rnd = new Random(seed);
		int n = rnd.nextInt(size * size);//randomize the grid a random number of times
		for (int i = 0; i < n; i++) {
			choose(rnd.nextInt(size * size));
		}
	}

	/**
	 * Resets the grid to all the bits being turned off
	 */
	private void resetGrid() {
		for (int i = 0; i < grid.length; i++) {
			grid[i] = false;
		}
		updateButtons();
	}

	/**
	 * Prints the grid to the output stream in the form:
	 * <p>
	 * __A B C 1 0 1 0 2 1 1 1 3 0 1 0
	 */
	private void printGrid() {
		print("    ");
		for (int i = 65; i < size + 65; i++) {
			print((char) i + " ");
		}
		for (int y = 0; y < size; y++) {
			print("\n" + as2Digits(y + 1) + "  ");//column label
			for (int x = 0; x < size; x++) {
				int bit = grid[x + y * size] ? 1 : 0;
				print(bit + " ");
			}
		}
		println();
	}

	/**
	 * Returns a number less than 100 as a number with 2 digits so '3' becomes
	 * '03'
	 * <p>
	 * @param num the number to return as 2 digits
	 * <p>
	 * @return <b>num<b> as 2 digits
	 */
	private String as2Digits(int num) {
		return (num < 10 ? " " : "") + num;
	}

	/**
	 * Updates the colour and text on the buttons when a button is pressed
	 */
	private void updateButtons() {
		for (int y = 0; y < size; y++) {
			for (int x = 0; x < size; x++) {
				jButton[x][y].setText(grid[x + y * size] ? "1" : "0");
				jButton[x][y].setBackground(grid[x + y * size] ? onColour : offColour);
			}
		}
	}

	/**
	 * All the GUI is made here
	 */
	private void initComponents() {
		initButtons();
		println("Size: " + size);
		this.setLayout(new GridLayout(size, size));
		for (int y = 0; y < size; y++) {
			for (int x = 0; x < size; x++) {
				this.add(jButton[x][y]);
			}
		}
	}

	/**
	 * Creates the buttons and sets them up with the correct action listeners
	 */
	private void initButtons() {
		jButton = new JButton[size][size];

		for (int y = 0; y < size; y++) {
			for (int x = 0; x < size; x++) {
				jButton[x][y] = new JButton();
				jButton[x][y].setName("" + (x + y * size));
				jButton[x][y].setFont(new Font("sansserif", Font.BOLD, 24));
				jButton[x][y].addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						JButton button = (JButton) e.getSource();//get the button that was pressed
						println("Button: " + button.getName());
						choose(Integer.parseInt(button.getName()));//the buttons are named in the same way
						printGrid();                               // the grid array is layed out
						updateButtons();
						checkWin();
					}
				});
				//as buttons are always in focus, add keylistener to them and not the unclickable frame/panel
				jButton[x][y].addKeyListener(new KeyListener() {
					@Override
					public void keyTyped(KeyEvent e) {
					}

					@Override
					public void keyPressed(KeyEvent e) {
						if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
							System.exit(0);
						}
					}

					@Override
					public void keyReleased(KeyEvent e) {
					}

				});
			}
		}
	}

	/**
	 * Prints a new line
	 */
	private void println() {
		println("");
	}

	/**
	 * Prints the <b>obj</b> followed by a new line
	 * <p>
	 * @param obj the object to print, calls the <tt>.toString()</tt> method
	 */
	private void println(Object obj) {
		print(obj + "\n");
	}

	/**
	 * Having one point where everything is printed to the terminal makes it
	 * easy to turn on and off, and do clever things with it
	 * <p>
	 * @param obj the object to print to the terminal, or the print stream
	 */
	private void print(Object obj) {
		if (ps != null) {
			ps.print(obj);
		}
		if (terminal) {//prints to the fake terminal if its visible, or the normal one if it is not
			if (null != Startup.terminal && Startup.terminal.isVisible()) {
				Startup.terminal.print(obj);
			} else {
				System.out.print(obj);
			}
		}
	}

}
