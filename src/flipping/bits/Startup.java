package flipping.bits;

import java.awt.Color;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.EventObject;
import javax.swing.JCheckBox;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

/**
 * Small GUI to choose multiple options for the game
 * <p>
 * @author Oliver Olding
 */
public class Startup extends JFrame {

	private static boolean RANDOM = true, TERMINAL = false;
	private static int SIZE = 5;
	/**
	 * The seed used for the random function, it takes the current time in
	 * nanoseconds and milliseconds and does some random stuff with it
	 */
	public static long SEED = System.nanoTime() * System.currentTimeMillis() % ((long) Math.exp(10));
	private static Color ONCOLOUR = Color.ORANGE;
	private static Color OFFCOLOUR = Color.YELLOW;
	private static PrintStream PS;
	/**
	 * The fake terminal application because you can`t open a real terminal to
	 * print to with code
	 */
	public static Terminal terminal;
	private String laf;

	/**
	 * Creates new form NewJFrame
	 */
	public Startup() {
		initComponents();
		this.setLocation(500, 300);
		//<editor-fold defaultstate="collapsed" desc="Set colours of buttons">
		onColourButton.setBackground(ONCOLOUR);
		onColourText.setText("0x" + Integer.toHexString(ONCOLOUR.getRGB()).substring(2).toUpperCase());
		offColourButton.setBackground(OFFCOLOUR);
		offColourText.setText("0x" + Integer.toHexString(OFFCOLOUR.getRGB()).substring(2).toUpperCase());
		//</editor-fold>
		seedText.setText(SEED + "");
	}

	private void startFlippy() {
		JFrame frame = new JFrame("Flippy Bits");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		//<editor-fold defaultstate="collapsed" desc="Open terminal and print to file">
		if (TERMINAL) {
			// if the program wasn't started by the command line
			if(null == System.console()){
				terminal = new Terminal();
				terminal.setVisible(true);
				terminal.print(FlippingBitsStart.getVersion() + "\n\n");
			}
		}
		if (fileCheckBox.isSelected()) {
			String path = fileText.getText();
			try {//if there was no filename given, default to "output.txt"
				PS = new PrintStream(path.isEmpty() ? "output.txt" : path);
			} catch (FileNotFoundException e) {
				System.err.println("Error: " + e);
			}
		} else {
			PS = null;
		}
		//</editor-fold>
		FlippingBits fb = new FlippingBits(RANDOM, TERMINAL, PS, ONCOLOUR, OFFCOLOUR, SIZE, SEED);
		frame.setContentPane(fb);
		frame.pack();
		frame.setVisible(true);
		frame.setLocation(this.getLocation());
	}

	/**
	 * This method is called from within the constructor to initialise the form.
	 * WARNING: Do NOT modify this code. The content of this method is always
	 * regenerated by the Form Editor.
	 */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        sizeSlider = new javax.swing.JSlider();
        jLabel1 = new javax.swing.JLabel();
        sizeText = new flipping.bits.JIntegerField();
        randomiseCheckBox = new javax.swing.JCheckBox();
        terminalCheckBox = new javax.swing.JCheckBox();
        fileCheckBox = new javax.swing.JCheckBox();
        fileText = new javax.swing.JTextField();
        onColourButton = new javax.swing.JButton();
        offColourButton = new javax.swing.JButton();
        offColourText = new javax.swing.JTextField();
        onColourText = new javax.swing.JTextField();
        goButton = new javax.swing.JButton();
        seedText = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        String[] lookAndFeels = new String[UIManager.getInstalledLookAndFeels().length];

        for(int i = 0; i < UIManager.getInstalledLookAndFeels().length; i++){
            lookAndFeels[i] = UIManager.getInstalledLookAndFeels()[i].getName();
        }
        lookAndFeelCombo = new javax.swing.JComboBox(lookAndFeels);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Flippy Bits Settings");
        setAlwaysOnTop(true);
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        setFocusable(false);
        setName("startupPage"); // NOI18N
        setResizable(false);

        sizeSlider.setMinimum(1);
        sizeSlider.setValue(5);
        sizeSlider.addMouseWheelListener(new java.awt.event.MouseWheelListener() {
            public void mouseWheelMoved(java.awt.event.MouseWheelEvent evt) {
                sizeSliderMouseWheelMoved(evt);
            }
        });
        sizeSlider.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                sizeSliderStateChanged(evt);
            }
        });

        jLabel1.setText("Size:");

        sizeText.setText("5");
        sizeText.addMouseWheelListener(new java.awt.event.MouseWheelListener() {
            public void mouseWheelMoved(java.awt.event.MouseWheelEvent evt) {
                sizeTextMouseWheelMoved(evt);
            }
        });
        sizeText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sizeTextActionPerformed(evt);
            }
        });
        sizeText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                sizeTextKeyReleased(evt);
            }
        });

        randomiseCheckBox.setSelected(true);
        randomiseCheckBox.setText("Randomise the gird");
        randomiseCheckBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                randomiseCheckBoxActionPerformed(evt);
            }
        });

        terminalCheckBox.setText("Output to terminal");
        terminalCheckBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                terminalCheckBoxActionPerformed(evt);
            }
        });

        fileCheckBox.setText("Output to file");
        fileCheckBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fileCheckBoxActionPerformed(evt);
            }
        });

        fileText.setEditable(false);
        fileText.setEnabled(false);

        onColourButton.setFont(new java.awt.Font("SansSerif", 1, 24)); // NOI18N
        onColourButton.setText("1");
        onColourButton.setMaximumSize(new java.awt.Dimension(40, 40));
        onColourButton.setMinimumSize(new java.awt.Dimension(40, 40));
        onColourButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                onColourButtonActionPerformed(evt);
            }
        });

        offColourButton.setFont(new java.awt.Font("SansSerif", 1, 24)); // NOI18N
        offColourButton.setText("0");
        offColourButton.setMaximumSize(new java.awt.Dimension(40, 40));
        offColourButton.setMinimumSize(new java.awt.Dimension(40, 40));
        offColourButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                offColourButtonActionPerformed(evt);
            }
        });

        offColourText.setEditable(false);

        onColourText.setEditable(false);

        goButton.setText("GO");
        goButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                goButtonActionPerformed(evt);
            }
        });

        seedText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                seedTextActionPerformed(evt);
            }
        });

        jLabel2.setText("Seed");

        lookAndFeelCombo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                lookAndFeelComboActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(terminalCheckBox)
                    .addComponent(lookAndFeelCombo, javax.swing.GroupLayout.PREFERRED_SIZE, 146, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(onColourButton, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(offColourButton, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(seedText)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel2)
                                .addGap(0, 0, Short.MAX_VALUE))))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(sizeSlider, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(randomiseCheckBox, javax.swing.GroupLayout.Alignment.LEADING))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(sizeText, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(fileCheckBox)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(fileText, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(onColourText, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(offColourText, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(goButton, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(sizeSlider, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(randomiseCheckBox))
                    .addComponent(sizeText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(terminalCheckBox)
                .addGap(4, 4, 4)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(fileCheckBox)
                    .addComponent(fileText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(onColourButton, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(offColourButton, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel2)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(59, 59, 59)
                        .addComponent(seedText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(onColourText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(offColourText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(goButton))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lookAndFeelCombo, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void fileCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fileCheckBoxActionPerformed
		boolean isSelected = fileCheckBox.isSelected();
		fileText.setEnabled(isSelected);
		fileText.setEditable(isSelected);
    }//GEN-LAST:event_fileCheckBoxActionPerformed

    private void sizeSliderStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_sizeSliderStateChanged
		JSlider slider = (JSlider) evt.getSource();
		SIZE = slider.getValue();
		sizeText.setText(SIZE + "");
    }//GEN-LAST:event_sizeSliderStateChanged

    private void sizeTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sizeTextActionPerformed
		JIntegerField text = (JIntegerField) evt.getSource();
		sizeSlider.setValue(text.getInt());
    }//GEN-LAST:event_sizeTextActionPerformed

    private void onColourButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_onColourButtonActionPerformed
		Color tempColour = JColorChooser.showDialog(this, "Choose on colour", ONCOLOUR);
		if (tempColour != null) {
			ONCOLOUR = tempColour;
		}
		setButtonColours();
		setButtonText();
    }//GEN-LAST:event_onColourButtonActionPerformed

    private void randomiseCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_randomiseCheckBoxActionPerformed
		JCheckBox check = (JCheckBox) evt.getSource();
		boolean isSelected = check.isSelected();
		RANDOM = isSelected;
		seedText.setEditable(isSelected);
		seedText.setEnabled(isSelected);
    }//GEN-LAST:event_randomiseCheckBoxActionPerformed

    private void terminalCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_terminalCheckBoxActionPerformed
		JCheckBox check = (JCheckBox) evt.getSource();
		TERMINAL = check.isSelected();
    }//GEN-LAST:event_terminalCheckBoxActionPerformed

    private void offColourButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_offColourButtonActionPerformed
		Color tempColour = JColorChooser.showDialog(this, "Choose off colour", OFFCOLOUR);
		if (tempColour != null) {
			OFFCOLOUR = tempColour;
		}
		setButtonColours();
		setButtonText();
    }//GEN-LAST:event_offColourButtonActionPerformed

    private void goButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_goButtonActionPerformed
		startFlippy();
		this.dispose();
    }//GEN-LAST:event_goButtonActionPerformed

    private void sizeSliderMouseWheelMoved(java.awt.event.MouseWheelEvent evt) {//GEN-FIRST:event_sizeSliderMouseWheelMoved
		JSlider slide = (JSlider) evt.getSource();
		slide.setValue(slide.getValue() - evt.getWheelRotation());
		SIZE = slide.getValue();
		sizeText.setText(SIZE + "");//only accepts string
    }//GEN-LAST:event_sizeSliderMouseWheelMoved

    private void sizeTextMouseWheelMoved(java.awt.event.MouseWheelEvent evt) {//GEN-FIRST:event_sizeTextMouseWheelMoved
		sizeSlider.setValue(sizeSlider.getValue() - evt.getWheelRotation());
		SIZE = sizeSlider.getValue();
		sizeText.setText(SIZE + "");
    }//GEN-LAST:event_sizeTextMouseWheelMoved

    private void seedTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_seedTextActionPerformed
		setSeed(evt);
    }//GEN-LAST:event_seedTextActionPerformed

    private void sizeTextKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_sizeTextKeyReleased
		JTextField text = (JTextField) evt.getSource();
		try {
			sizeSlider.setValue(Integer.parseInt(text.getText()));
		} catch (NumberFormatException e) {
			System.err.println(e);
		}
    }//GEN-LAST:event_sizeTextKeyReleased
	//only used in next method, need somewhere to store previous selected item

	private int prevSelection = 0;

    private void lookAndFeelComboActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_lookAndFeelComboActionPerformed
		JComboBox jcb = (JComboBox) evt.getSource();
		laf = UIManager.getInstalledLookAndFeels()[jcb.getSelectedIndex()].getClassName();

		if (UIManager.getInstalledLookAndFeels()[jcb.getSelectedIndex()].getName().equals("Windows")) {
			int opt = JOptionPane.showConfirmDialog(null, "Windows look and feel doesn't support\n"
					+ "coloured buttons, continue?", "Warning", JOptionPane.YES_NO_OPTION);
			if (opt == JOptionPane.NO_OPTION) {//if no
				lookAndFeelCombo.setSelectedIndex(prevSelection);
				return;
			}
		}

		try {
			UIManager.setLookAndFeel(laf);
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException |
				UnsupportedLookAndFeelException e) {
			System.err.println("Error: " + e);
		}
		SwingUtilities.updateComponentTreeUI(this);
		pack();
		prevSelection = jcb.getSelectedIndex();
    }//GEN-LAST:event_lookAndFeelComboActionPerformed

	private void setButtonColours() {
		onColourButton.setBackground(ONCOLOUR);
		offColourButton.setBackground(OFFCOLOUR);
	}

	private void setButtonText() {
		//getRGB() returns in the form ARGB so substring(2) condenses it to RGB
		String onHex = Integer.toHexString(ONCOLOUR.getRGB());
		onColourText.setText("0x" + onHex.substring(2).toUpperCase());
		String offHex = Integer.toHexString(OFFCOLOUR.getRGB());
		offColourText.setText("0x" + offHex.substring(2).toUpperCase());
	}

	private void setSeed(EventObject evt) {
		JTextField text = (JTextField) evt.getSource();
		try {
			if (!text.getText().isEmpty()) {
				SEED = Integer.parseInt(text.getText());
			}
		} catch (NumberFormatException e) {
			System.err.println("Error: " + e);
		}
	}
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JCheckBox fileCheckBox;
    private javax.swing.JTextField fileText;
    private javax.swing.JButton goButton;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JComboBox lookAndFeelCombo;
    private javax.swing.JButton offColourButton;
    private javax.swing.JTextField offColourText;
    private javax.swing.JButton onColourButton;
    private javax.swing.JTextField onColourText;
    private javax.swing.JCheckBox randomiseCheckBox;
    private javax.swing.JTextField seedText;
    private javax.swing.JSlider sizeSlider;
    private javax.swing.JTextField sizeText;
    private javax.swing.JCheckBox terminalCheckBox;
    // End of variables declaration//GEN-END:variables
}
